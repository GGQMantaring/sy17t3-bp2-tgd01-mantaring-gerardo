#include <iostream>
#include <string>
#include <time.h>		//These three are for randomization
#include <stdio.h>
#include <stdlib.h>
#include <windows.h>	//This is for the timer

using namespace std;

//Required Node struct
struct Node
{
	string name;
	Node* next = NULL;
	Node* previous = NULL;
};

int main();

//Circle of soldiers exists here
class circleOfSoldiers		//really hoping this isn't classified as an "object container"
{
	Node* head, *tail;
public:
	circleOfSoldiers()
	{
		head = NULL;
		tail = NULL;
	}

	void createNode(string name)
	{		
		Node* temp = new Node;	//Creates new soldier
		temp->name = name;
		temp->next = head;

		if (head == NULL)	//puts node into list, creates a starting point
		{
			head = temp;
			tail = temp;
			temp = NULL;
		}
		else
		{
			tail->next = temp;	//makes tail point back to head
			tail = temp;		//tail now contains new node
			head->previous = tail;
		}
	}

	void displaySoldiers()
	{
		Node *current = new Node;
		Node *temp = new Node;

		current = head;				//start at head

		do	
		{
			cout << current->name << endl;
			temp = current;
			current = current->next;
		} while (current != head);		//keep going until current node is back at head

	}

	void passCloak(int remainingSoldiers)
	{
		srand(time(NULL));
		int random = rand()% remainingSoldiers + 1;

		Node* temp = new Node;

		cout << endl << head->name << " drew a " << random << endl;

		temp = head;
		for (int i = 0; i < random; i++)	//moves down circle 
		{
			cout << "previously: " << head->name << endl;
			head = head->next;
			cout << "currently: " << head->name << endl;
		}
	}

	void removeSoldier()
	{
		cout << endl << head->name << " was eliminated";
		Node *toDelete;
		toDelete = head;

		// Loop through the entire list to get the previous
		Node *current = head;
		do {
			current = current->next;
		} while (current->next != toDelete);
		current->next = toDelete->next;
		
		delete toDelete;
	}

	void gameCylce(int remainingSoldiers)
	{
		displaySoldiers();		
		passCloak(remainingSoldiers);
		removeSoldier();
		cout << endl << endl;
		
	}

	void finalResult()
	{
		cout << R"_(_ | ___ | ___ | ___ | ___ | ___ | ___ | ___ | _|)_" << endl;
		cout << R"_( ___ | ___ | ___ | ___ | ___ | ___ | ___ | ___ |)_" << endl << endl;
		cout << "F I N A L   R E S U L T S" << endl << endl;
		cout << R"_( ___ | ___ | ___ | ___ | ___ | ___ | ___ | ___ |)_" << endl << endl;
		cout << head->name << " will go out to look for reinforcements" << endl << endl;
	}

} circle;

int main()		
{
	int population, counter = 1;
	string name;

	//title screen
	cout << R"_(_ | ___ | ___ | ___ | ___ | ___ | ___ | ___ | _|)_" << endl;
	cout << R"_( ___ | ___ | ___ | ___ | ___ | ___ | ___ | ___ |)_" << endl;
	cout << R"_(_ | ___ |  __|_____|_____|_____|_____| ___ | __|)_" << endl;
	cout << R"_( ___ | ___|    _____ __  __ _____     |_ | ____|)_" << endl;
	cout << R"_(_ | ___ | |     ||   ||==|| ||==      | ___ | _|)_" << endl;
	cout << R"_( ___ | ___|     ||   ||  || ||___     |_ | ____|)_" << endl;
	cout << R"_(_ | ___ | | __    __ ___  __    __    | ___ | _|)_" << endl;
	cout << R"_( ___ | ___| \\ /\ //||=|| ||    ||    |_ | ____|)_" << endl;
	cout << R"_(_ | ___ | |  \V/\V/ || || ||__| ||__| | ___ | _|)_" << endl;
	cout << R"_( ___ | ___|___________________________|_ | ____|)_" << endl;
	cout << R"_(_ | ___ | ___ | ___ | ___ | ___ | ___ | ___ | _|)_" << endl;
	cout << R"_( ___ | ___ | ___ | ___ | ___ | ___ | ___ | ___ |)_" << endl;
	Sleep(4000);
	system("cls");

	cout << R"_(_ | ___ | ___ | ___ | ___ | ___ | ___ | ___ | _)_" << endl;
	cout << R"_( ___ | ___ | ___ | ___ | ___ | ___ | ___ | ___ )_" << endl << endl;
	cout << "How many soldiers are on the Wall? ";
	cin >> population;		//determine size of population
	system("cls");

	if (population == 0)	//hard-coded null state (bad end 1)
	{
		cout << R"_(_ | ___ | ___ | ___ | ___ | ___ | ___ | ___ | _|)_" << endl;
		cout << R"_( ___ | ___ | ___ | ___ | ___ | ___ | ___ | ___ |)_" << endl << endl;
		cout << "There are no soldiers? Then the land is doomed. . ." << endl << endl;
		cout << R"_(_ | ___ |  __|_____|_____|_____|_____| ___ | __|)_" << endl;
		cout << R"_( ___ | ___| ____   ___  ___  __ _____ |_ | ____|)_" << endl;
		cout << R"_(_ | ___ | |(( ___ ||=|| || \/ | ||==  | ___ | _|)_" << endl;
		cout << R"_( ___ | ___| \\_|| || || ||    | ||___ |_ | ____|)_" << endl;
		cout << R"_(_ | ___ | |  _____  __ __ _____ _____ | ___ | _|)_" << endl;
		cout << R"_( ___ | ___| ((   )) \\ // ||==  ||_// |_ | ____|)_" << endl;
		cout << R"_(_ | ___ | |  \\_//   \V/  ||___ || \\ | ___ | _|)_" << endl;
		cout << R"_( ___ | ___|___________________________|_ | ____|)_" << endl;
		cout << R"_(_ | ___ | ___ | ___ | ___ | ___ | ___ | ___ | _|)_" << endl << endl;
	}
	else if (population == 1)	//more hardcoded stuff (bad end 2)
	{
		cout << R"_(_ | ___ | ___ | ___ | ___ | ___ | ___ | ___ | _|)_" << endl;
		cout << R"_( ___ | ___ | ___ | ___ | ___ | ___ | ___ | ___ |)_" << endl << endl;		
		cout << "There is only one soldier so the Lord Commander sends him off immediately." << endl;
		cout << "The Lord Commander has no choice but to defend the Wall by himself." << endl;
		cout << "He only prays that the only watchman returns immediately with help." << endl << endl;
		cout << R"_(_ | ___ |  __|_____|_____|_____|_____| ___ | __|)_" << endl;
		cout << R"_( ___ | ___| ____   ___  ___  __ _____ |_ | ___ |)_" << endl;
		cout << R"_(_ | ___ | |(( ___ ||=|| || \/ | ||==  | ___ | _|)_" << endl;
		cout << R"_( ___ | ___| \\_|| || || ||    | ||___ |_ | ___ |)_" << endl;
		cout << R"_(_ | ___ | |  _____  __ __ _____ _____ | ___ | _|)_" << endl;
		cout << R"_( ___ | ___| ((   )) \\ // ||==  ||_// |_ | ___ |)_" << endl;
		cout << R"_(_ | ___ | |  \\_//   \V/  ||___ || \\ | ___ | _|)_" << endl;
		cout << R"_( ___ | ___|___________________________|_ | ___ |)_" << endl;
		cout << R"_(_ | ___ | ___ | ___ | ___ | ___ | ___ | ___ | _|)_" << endl << endl;
	}
	else
	{
		for (int i = 1; i <= population; i++)	//populate circle
		{
			cout << R"_(_ | ___ | ___ | ___ | ___ | ___ | ___ | ___ | _|)_" << endl;
			cout << R"_( ___ | ___ | ___ | ___ | ___ | ___ | ___ | ___ |)_" << endl << endl;
			if (i == 1)
			{
				cout << "What is this soldier's name? ";
			}
			else if (i == population)
			{
				cout << "And finally, who is the last soldier? ";
			}
			else
			{
				cout << "And who is this next one? ";
			}

			cin >> name;
			circle.createNode(name);	//node created per soldier specified
			cout << endl;
		}

		system("cls");
		cout << R"_(_ | ___ | ___ | ___ | ___ | ___ | ___ | ___ | _)_" << endl;
		cout << R"_( ___ | ___ | ___ | ___ | ___ | ___ | ___ | ___ )_" << endl << endl;
		cout << "The soldiers now pray to the gods that the Fates be kind . . ." << endl << endl;
		Sleep(2000);

		do
		{
			cout << R"_(_ | ___ | ___ | ___ | ___ | ___ | ___ | ___ | _|)_" << endl;
			cout << R"_( ___ | ___ | R O U N D )_" << counter << R"_( ___ | ___ | ___ | ___ |)_" << endl << endl;
			circle.gameCylce(population);
			counter++;
			population--;
			system("pause");
		} while (population != 1);

		circle.finalResult();
		
	}

	
	system("pause");
	return 0;
}