#include <iostream>
#include <ctime>
#include <string>

using namespace std;

class Spell
{
public:
	string name;
	int cost;
	int dmg;
};

class Wizard
{
public:
	string name;
	int hp;
	int mp;
	bool canGoOn = true;

	void castSpell(Spell* spell, Wizard* enemy)
	{
		if (this->hp > 0 && this->mp > 0)
		{
			cout << this->name << " casts " << spell->name << "." << endl;
			this->mp -= spell->cost;
			enemy->hp -= spell->dmg;
			cout << this->name << endl << "HP: " << this->hp << endl << "MP: " << this->mp << endl << endl;
			cout << enemy->name << endl << "HP: " << enemy->hp << endl << "MP: " << enemy->mp << endl << endl;
			cout << endl << endl;
		}
		else
		{
			canGoOn = false;
		}
	}
};

void assignWizardValues(Wizard* wizard, Spell* spell)
{
	wizard->hp = rand() % 25 + 76;
	wizard->mp = rand() % 25 + 76;
	spell->name = "Fireball";
	spell->cost = rand() % 5 + 6;
	spell->dmg = rand() % 5 + 6;
}

int main()
{
	Wizard* wiz1 = new Wizard();
	Wizard* wiz2 = new Wizard();
	Spell* fireball1 = new Spell();
	Spell* fireball2 = new Spell();

	//Assign Property Values
	srand(time(NULL));
	cout << "Who is the first wizard?  ";
	cin >> wiz1->name;
	assignWizardValues(wiz1, fireball1);

	cout << "Who is the second Wizard?  ";
	cin >> wiz2->name;
	assignWizardValues(wiz2, fireball2);

	//Battle START!!

	do
	{
		wiz1->castSpell(fireball1, wiz2);
		wiz2->castSpell(fireball2, wiz1);
	} while (wiz1->canGoOn == true && wiz2->canGoOn == true);

	system("pause");
	return 0;
}