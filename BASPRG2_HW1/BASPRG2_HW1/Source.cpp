#include <iostream>
#include <string>

using namespace std;

long long int CalculateFactorial(unsigned int valueQuan)
{
	unsigned long long factorial = 1;

	for (int i = 1; i <= valueQuan; ++i)
	{
		factorial *= i;
	}

	return factorial;
}

int main()
{	
	int number = 0, valueOrig;
	long long int answer;
	string adjective, description;

	do
	{
		if (number < 21 && number >= 0)
		{
			cout << "Give me a number and I'll get its factorial. No negatives, okay?" << endl;
			cout << "Seriously, don't try it." << endl << endl;
			system("pause");
			system("cls");
			cout << "Oh, and just because I don't know how to stop you from entering a decimal doesn't mean I can't" << endl;
			cout << "choose to ignore it. I'll just grab whatever whole number you have there and disregard whatever" << endl;
			cout << "comes after the decimal point. So make it easy on us both and just type a whole number, please." << endl << endl;
		}
		else if (number < 0)
		{
			cout << "I have no space in my virtual life for that sort of negativity." << endl;
			cout << "Try again, buckaroo." << endl << endl;
		}
		else
		{
			cout << "Woah, hold on there. I can't hold that many bits of information. I'm sorry, but I can only " << endl;
			cout << "calculate the factorial of up to 20, so you're going to have to change that number." << endl << endl;
		}

		cout << "Factorial of: ";
		cin >> number;
		cout << endl;
		cout << "Okay, give me a second." << endl << endl;
		system("pause");
		system("cls");
	} while (number < 0 || number > 20);

	valueOrig = number;
	answer = CalculateFactorial(number);

	//Cheating, probably
	if (number == 0)
	{
		cout << "Oh, that's easy" << endl;
		cout << "0! = 1" << endl << endl;
		system("pause");
		return 0;
	}

	if (valueOrig < 11)
	{
		adjective = "weak";
		description = "Do you even lift, bruh?";
	}
	else
	{
		adjective = "swole";
		description = "That number's jacked, man!";
	}
	cout << "Your number is " << adjective << "." << endl << endl;
	cout << "Basically, " << valueOrig << "! is exactly " << answer << "." << endl << endl;
	cout << description << endl << endl;

	system("pause");
	return 0;
}