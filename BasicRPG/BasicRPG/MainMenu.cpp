#include <iostream>
#include "MainMenu.h"
#include "Character.h"
#include "Map.h"



MainMenu::MainMenu()
{
}


MainMenu::~MainMenu()
{
}

int MainMenu::chooseAction()
{
	int action = 0;

	cout << "(1)  Explore" << endl;
	cout << "(2)  Rest" << endl << endl;

	//Checks input
	while (action < 1 || action > 2)
	{
		cout << "What do you want to do?  ";
		cin >> action;
		cout << endl;

		if (action != 1 && action != 2)
		{
			cout << "That is not a valid option. Please try again." << endl << endl;
		}
		else if (action == 1)	//Move
		{
			return 1;
		}
		else if (action == 2)	//Rest
		{
			return 2;
		}
	}
}

int MainMenu::chooseCombatAction()
{
	int action = 0;

	cout << "(1)  Attack" << endl;
	cout << "(2)  Flee" << endl << endl;

	//Checks input
	while (action < 1 || action > 2)
	{
		cout << "What do you want to do?  ";
		cin >> action;

		if (action != 1 && action != 2)
		{
			cout << "That is not a valid option. Please try again." << endl << endl;
		}
		else if (action == 1)	//Attack
		{
			return 1;
		}
		else if (action == 2)	//Flee
		{
			return 2;
		}
	}
}
