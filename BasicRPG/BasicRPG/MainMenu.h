#pragma once
#include <iostream>
#include <string>

class Character;
class Map;

using namespace std;

class MainMenu
{

public:
	MainMenu();
	~MainMenu();

	int chooseAction();
	int chooseCombatAction();
};