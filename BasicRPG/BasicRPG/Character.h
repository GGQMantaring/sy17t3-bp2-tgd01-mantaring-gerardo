#pragma once
#include <string>
class Map;

using namespace std;

class Character
{
	int  LVL, EXP, HP, POW, VIT, AGI, DEX;
	
	//Stat assignment functions
	void assignStats();

	//Battle functions
	int dealsDamage(Character* defender);
	bool confirmHitAgainst(Character* defender);
	void gainExpFrom(Character* enemy);

public:
	Character();
	~Character();

	string name, jobName;
	int currentHP, currentEXP;
	int jobID;
	bool combatPhase;
	bool canGoOn;

	void display();

	//Create unit
	void createUnit();
	void adjustToEnemy(Character* player, Map* map);	//Function for monster
	
	//Main Menu functions
	void move(Map* map);
	void rest();

	//Battle functions
	void attack(Character* defender);
	void checkLevelUp();
};

