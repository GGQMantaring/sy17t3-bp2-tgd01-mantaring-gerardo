#include <iostream>
#include <ctime>
#include "Character.h"
#include "Map.h"

using namespace std;

Map::Map()
{
}


Map::~Map()
{
}

void Map::spawner(Character* player)
{
	//Monster spawn rate
	monsterID = rand() & 20 + 1;

	
	 if (monsterID >= 16 && monsterID == 20)
	{
		cout << "A monster did not appear. It's safe to keep going, I suppose." << endl << endl;
		system("pause");
		system("cls");
		player->combatPhase = false;
		return;
	}
	 else
	 {
		 cout << "The ground RUMBLES!! An opponent appears ready to fight!" << endl << endl;
		 system("pause");
		 system("cls");
		 player->combatPhase = true;
	 }	
}

void Map::displayLocation()
{
	cout << "Current Location:   (" << this->x << "," << this->y << ")";
}
