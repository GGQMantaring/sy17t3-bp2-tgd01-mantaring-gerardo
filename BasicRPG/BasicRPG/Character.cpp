#include <iostream>
#include <ctime>
#include <cstdlib>
#include "Character.h"
#include "Map.h"

using namespace std;

Character::Character()
{
	//Assigning base values
	HP = 10;
	POW = 10;
	VIT = 10;
	AGI = 10;
	DEX = 10;
}


Character::~Character()
{
}
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    
void Character::display()
{
	cout << this->name << ", the " << this->jobName << "     LVL: " << this->LVL << endl;
	cout << " HP:    " << this->currentHP << "/" << this->HP << endl;
	cout << "EXP:    " << this->currentEXP << " / " << this->EXP << endl;
	cout << "POW:    " << this->POW << endl;
	cout << "VIT:    " << this->VIT << endl;
	cout << "AGI:    " << this->AGI << endl;
	cout << "DEX:    " << this->DEX << endl << endl;
}

void Character::createUnit()
{
	cout << "Hi, Non-Specified Gender Neutral Identifier! You are not playing a game!" << endl;
	cout << "YOU ARE IN A BATTLE OF LIFE AND DEATH!" << endl;
	cout << "What name do we write on your tombstone? ";
	cin >> this->name;
	cout << endl << endl;
	cout << "       (1) Warrior" << endl;
	cout << "       (2) Mage" << endl;
	cout << "       (3) Cleric" << endl;
	cout << "       (4) Thief" << endl << endl;

	//Checks input
	while (this->jobID < 1 || this->jobID > 4)
	{
		cout << "Now then, what is your profession? (ENTER A NUMBER)  ";
		cin >> this->jobID;

		if (this->jobID < 1 || this->jobID > 4)
		{
			cout << "That is not a valid option. Please try again." << endl << endl;
		}
		else
		{
			this->assignStats();
			cout << endl << "Splendid! You are a " << this->jobName << ". Good luck with your quest." << endl;
			cout << "May Death come quick to whom it chooses. Do not forget to offer each corpse to The RNG." << endl << endl;
			system("pause");
			system("cls");
		}
	}
}

void Character::assignStats()
{
	if (jobID == 1)
	{
		this->jobName = "Warrior";
		this->POW += 2;
		this->VIT += 4;
	}
	else if (jobID == 2)
	{
		this->jobName = "Mage";
		this->POW += 4;
		this->VIT += 2;
	}
	else if (jobID == 3)
	{
		this->jobName = "Cleric";
		this->HP += 4;
		this->VIT += 2;
	}
	else if (jobID == 4)
	{
		this->jobName = "Thief";
		this->AGI += 4;
		this->VIT += 2;
	}

	this->canGoOn = true;
	this->LVL = 1;
	this->currentHP = this->HP;
	this->EXP = this->LVL * 1000;
	this->currentEXP = 0;
}

void Character::adjustToEnemy(Character * player, Map* map)
{
	this->jobID = map->monsterID;
	//Name randomizer
	string firstName[] = { "Lok", "Horr", "Xurin", "Zed", "Kean", "Lig", "Zul", "Duran", "Debbie" };
	string lastName[] = { "Ma", "Tarr", "Huron", "Baron", "Uruk", "Jinn", "Garrok", "Vun", "Durn" };

	//Assigning name to enemy
	if (this->jobID == 1)
	{
		this->name = "Orc Lord " + firstName[rand() % 9] + R"_(')_" + lastName[rand() % 9];
	}
	else if (this->jobID >= 2 && this->jobID <= 6)
	{
		this->name = "Orc, " + firstName[rand() % 9] + R"_(')_" + lastName[rand() % 9];
		this->jobID = 2;		//Changes here and below are to definitively identify enemy through jobID for EXP calculations
	}
	else if (this->jobID >= 7 && this->jobID <= 11)
	{
		this->name = "Ogre, " + firstName[rand() % 9] + R"_(')_" + lastName[rand() % 9];
		this->jobID = 3;
	}
	else if (this->jobID >= 12 && this->jobID <= 16)
	{
		this->name = "Goblin, " + firstName[rand() % 9] + R"_(')_" + lastName[rand() % 9];
		this->jobID = 4;
	}

	//Semi-Linearly scaling this to dungeon level
	this->currentHP = this->HP + (rand() % 4 + player->LVL);
	this->VIT += rand() % 3 + player->LVL;
	this->AGI += rand() % 3 + player->LVL;
	this->DEX += rand() % 3 + player->LVL;
	this->POW += rand() % 3 + player->LVL;
}

void Character::move(Map* map)
{
	int action = 0;

	cout << "(1)  North" << endl;
	cout << "(2)  East" << endl;
	cout << "(3)  West" << endl;
	cout << "(4)  South" << endl << endl;

	//Second input check
	while (action < 1 || action > 4)
	{
		cout << "Where do you want to go?  ";
		cin >> action;
		cout << endl;
		if (action < 1 || action > 4)
		{
			cout << "You can't move in the 4th dimension just yet. Give another direction." << endl << endl;
		}
		else if (action == 1)	//North
		{
			cout << "You decide to head north this time. . ." << endl << endl;
			map->y += 1;
		}
		else if (action == 2)	//East
		{
			cout << "You shimmy over towards east. . ." << endl << endl;
			map->x += 1;
		}
		else if (action == 3)	//West
		{
			cout << "You turn around and start walking to the west. . ." << endl << endl;
			map->x -= +1;
		}
		else if (action == 4)	//South	
		{
			cout << "You turn and travel south a bit. . ." << endl << endl;
			map->y -= 1;
		}
	}
	system("pause");
}

void Character::rest()
{
	cout << "You decide to make camp here. It's not too bad, and the corpse of your last" << endl;
	cout << "foe is meaty enough to provide a good pillow for your head." << endl;
	cout << "In the morning, you feel revitalized!" << endl << endl;
	cout << "-----YOU RECOVERED ALL YOUR HEALTH-----" << endl << endl;
	this->currentHP = this->HP;
	system("pause");
	this->combatPhase = false;  //Placed this here for safety purposes
}

int Character::dealsDamage(Character * defender)
{
	int dmg = this->POW - defender->VIT;

	if (dmg < 1)
	{
		dmg = 1;
	}

	return dmg;
}

bool Character::confirmHitAgainst(Character * defender)
{
	int accuracy = (this->DEX / defender->AGI) * 100;

	if (accuracy < 20)
	{
		accuracy = 20;
	}
	else if (accuracy > 80)
	{
		accuracy = 80;
	}
		//Chance roll
	int chance = rand() % 100 + 1;

	//Compare chance and accuracy
	if (chance >= accuracy)
	{
		return true;
	}
	else
	{
		return false;
	}
}

void Character::attack(Character * defender)
{
	string attackVerb[] = { "strikes","attacks","ballyhoos","hits","jimmyjams" };
	int damage = this->dealsDamage(defender);
	
	if (this->currentHP > 0)
	{
		//Attack flow
		if (this->confirmHitAgainst(defender) == true)
		{
			cout << this->name << " " << attackVerb[(rand() % 5)] << " " << defender->name << "!" << endl;
			cout << "It deals " << damage << "!" << endl << endl;
			defender->currentHP -= damage;
			system("pause");
			cout << endl << endl;
		}
		else
		{
			cout << "O O F , it's a terrible miss!" << endl << endl;
			system("pause");
			cout << endl << endl;
		}
	}
	else
	{
		canGoOn = false;
	}	
}

void Character::gainExpFrom(Character * monster)
{
	int gainedEXP;

	if (monster->jobID == 1)
	{
		gainedEXP = 1000;
		cout << "You gained 1000 EXP for defeating such a large opponent!" << endl << endl;
		this->currentEXP += gainedEXP;
	}
	else if (monster->jobID == 2)
	{
		gainedEXP = 500;
		cout << "You gained 500 EXP for defeating a sizeable monster!" << endl << endl;
		this->currentEXP += gainedEXP;
	}
	else if (monster->jobID == 3)
	{
		gainedEXP = 250;
		cout << "You gained 250 EXP for killing it!" << endl << endl;
		this->currentEXP += gainedEXP;
	}
	else if (monster->jobID == 4)
	{
		gainedEXP = 100;
		cout << "Oh, hey. You gained 100 EXP. That's cool." << endl << endl;
		this->currentEXP += gainedEXP;
	}
}

void Character::checkLevelUp()
{
	//Check for level growth
	if (this->currentEXP == this->EXP)
	{
		int addedVal;
		cout << "--------------------------------------------------" << endl;
		cout << "You gained a level!" << endl << endl;
		this->LVL += 1;

		cout << "REACHED LEVEL " << this->LVL << endl << endl;

		//Add and Display values
		addedVal = rand() % 4;
		this->HP += addedVal;
		this->currentHP += addedVal;
		cout << "HP:  " << this->HP << "     ( +" << addedVal << " )" << endl;

		addedVal = rand() % 4;
		this->POW += addedVal;
		cout << "POW:  " << this->POW << "     ( +" << addedVal << " )" << endl;

		addedVal = rand() % 4;
		this->VIT += addedVal;
		cout << "VIT:  " << this->VIT << "     ( +" << addedVal << " )" << endl;

		addedVal = rand() % 4;
		this->AGI += addedVal;
		cout << "AGI:  " << this->AGI << "     ( +" << addedVal << " )" << endl;

		addedVal = rand() % 4;
		this->DEX += addedVal;
		cout << "DEX:  " << this->DEX << "     ( +" << addedVal << " )";

		//Readjust EXP goal
		this->currentEXP = 0;
		this->EXP = this->LVL * 1000;
	}
	cout << endl << endl;
}
