#include <iostream>
#include <ctime>
#include <string>
#include "Character.h"
#include "Map.h"
#include "MainMenu.h"

using namespace std;

void headerDisplay(Map* map, Character* player)
{
	cout << "--------------------------------------------------" << endl;
	//Player Display
	player->display();
	cout << endl << "--------------------------------------------------" << endl;
	//Map Display
	map->displayLocation();
	cout << endl << "--------------------------------------------------" << endl;
	cout << "--------------------------------------------------" << endl << endl;
}

int main()
{
	Map* map = new Map;
	MainMenu* menu = new MainMenu;
	
	srand(time(NULL));
	
	int action;

	//Create player
	Character* player = new Character;
	player->createUnit();

	player->combatPhase = false; //Start in non-combat
	
	do
	{
		//Header always displays on top
		headerDisplay(map, player);	//Displays status and location
		
		/*Main Menu
		combatPhase places player in either "Exploration State" or "Battle State"*/
		if (player->combatPhase == false)	//Exploration
		{
			action = menu->chooseAction();

			if (action == 1)	//Move
			{
				player->move(map);	//Function will move across map
				map->spawner(player);	//Checks if true for enemy, and if so sets combatPhase to true
			}
			else if (action == 2)	//Rest
			{
				player->rest();		//Heals to full HP and ensures combatPhase is still false
			}
		}
		else if (player->combatPhase == true)	//Battle
		{
			//Monster is created
			Character* monster = new Character;
			//Monster is scaled
			monster->adjustToEnemy(player, map);

			//Combat loop
			while (player->canGoOn == true && monster->canGoOn == true) //Combat will keep looping until either player or monster have less than 0 HP
			{
				action = menu->chooseCombatAction();

				//Choice to attack or flee
				if (action == 1)	//Attack
				{
					player->attack(monster);
					//Damage feedback
					cout << endl << "--------------------------------------------------" << endl;
					cout << "Opponent's remaining HP:  " << monster->currentHP << endl;
					cout << endl << "--------------------------------------------------" << endl << endl;
				}
				else if (action == 2)	//Flee
				{
					//Roll for flee = 25%
					action = rand() % 4;
					
					//Checks if flee is successful
					if (action == 0)	//Player flees
					{
						cout << "You make like a tree and get swept away by the hurricane of fear." << endl;
						cout << "You manage to escape though, so all is well." << endl << endl;
						cout << "-----YOU ESCAPED!-----" << endl << endl;
						system("pause");
						system("cls");
						player->combatPhase = false;	//Player is no longer in combat
					}
					else //Player fails to flee
					{
						cout << "You try to run, but you stupidly trip over your own weapon. Or a rock." << endl;
						cout << "Either way, the monster takes advantage of you and attacks!" << endl << endl;
						//Monster gets free hit on player
					}
				}				

				monster->attack(player);

			}

			//You win combat
			if (monster->canGoOn == false)
			{
				player->checkLevelUp();
				player->combatPhase = false;
				delete monster;
				system("pause");
			}
			//You lose combat (END GAME CONDITION)
			else if (player->canGoOn == false)
			{
				cout << "You died!" << endl << endl;
				system("pause");
				return 0;
			}
		}
		system("cls");

	} while (player->canGoOn == true);	//Game proper will loop until all HP is lost
}