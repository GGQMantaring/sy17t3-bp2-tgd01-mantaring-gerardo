//code from sir below:

#include <iostream>
#include <string>
#include <vector>
#include <ctime>

using namespace std;

void printVector(vector<string> cards)
{

}

int main()
{
	string cards[] = { "Emperor", "Citizen", "Citizen", "Citizen", "Citizen" };

	cout << cards[0] << endl; // Read
	cards[0] = "Slave"; // Write
						// Can't remove, need to make new array

						// vector<type> variableName;
	vector<string> cards2;
	// Add stuff to the vector
	cards2.push_back("A");
	cards2.push_back("B");
	cards2.push_back("C");
	cards2.push_back("D");
	cards2.push_back("E");

	cout << cards2[0] << endl; // Read
	cards2[0] = "Slave"; // Write

						 // Remove stuff
	cards2.pop_back();

	// Removing based on index
	int index = 3;
	cards2.erase(cards2.begin() + index);

	// Pass to a function
	printVector(cards2);

	system("pause");
	return 0;
}
