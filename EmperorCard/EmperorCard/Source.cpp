#include <iostream>
#include <string>
#include <vector>
#include <ctime>		//These three are for randomization
#include <stdio.h>
#include <stdlib.h>
#include <windows.h>	//This is for the timer

using namespace std;


struct Kaiji
{
	int millimeters = 30;
	unsigned long long int money = 0;
	int wager;
	bool empTurn = true;	//player always starts as Emperor in round 1
							//true = player is currently Emperor	
	int drawIndex;
	int turnCounter = 1;
	bool end = false;		//cheating by ending through convoluted intrinsic end state built into player
};

/*struct Cards
{
	vector<string> empCards;
	vector<string> slaveCards;
};*/

void winCheck(vector<string> empCards, vector<string> slaveCards, Kaiji player);

void wager(Kaiji player)
{
	cout << "How much length would you like to bet? (in mm)";
	cin >> player.wager;

	while (player.wager > player.millimeters || player.wager < 1);	//enters loop if bet is not allowed, stays until correct
	{		
		cout << "Error: wager is not acceptable. Bet again: ";
		cin >> player.wager;
		cout << endl;
	} 

	cout << endl << "You wagered " + player.wager << " millimeters." << endl << endl;
	system("pause");
	system("cls");
}

void receiveWinnings(Kaiji& player)
{
	int winnings;
	winnings = player.wager * 100000;
	
	cout << "Good job! You received ";
	
	if (player.empTurn == true)
	{
		cout << winnings;
	}
	else
	{
		cout << winnings * 5; //winnings*5 for playing Slave
	}	
		
	cout << " yen." << endl << endl;
	player.money += winnings;
	system("pause");
	system("cls");

	if (player.money >= 20000000)
	{
		cout << "You managed to win against Tonegawa! ! !" << endl;
		cout << "Your ear is intact, and you have the 20M yen!" << endl << endl;
		cout << "You have avenged your friends!" << endl << endl;

		cout << R"_( __   __   ___     ___    _____    ___     ___   __   __ )_" << endl;
		cout << R"_( \ \ / /  |_ _|   / __|  |_   _|  / _ \   | _ \  \ \ / / )_" << endl;
		cout << R"_(  \ V /    | |   | (__     | |   | (_) |  |   /   \ V /  )_" << endl;
		cout << R"_(   \_/    |___|   \___|    |_|    \___/   |_|_\    |_|   )_" << endl;
		cout << "[BEST ENDING]" << endl << endl;
		system("pause");
		player.end = true;
	}
}

void lostMatchup(Kaiji player)
{
	cout << "You lost the round! The drill moves " + player.wager << " millimeters deeper into your ear." << endl << endl;
	player.millimeters -= player.wager;	//wager subtracted from your remaining drill length
	
	system("pause");
	system("cls");

	if (player.millimeters <= 0)
	{
		cout << "The drill pierces through your eardrum! ! !" << endl;
		cout << "For a second, you can hear the wet sound of flesh being torn" << endl;
		cout << "apart by a drill. Then you hear nothing. " << endl;
		cout << "Warm blood trickles down your neck. You cry in pain." << endl << endl;

		cout << R"_(   ___     ___   __  __    ___              ___   __   __   ___     ___   )_" << endl;
		cout << R"_(  / __|   /   \ |  \/  |  | __|            / _ \  \ \ / /  | __|   | _ \  )_" << endl;
		cout << R"_( | (_ |   | - | | |\/| |  | _|            | (_) |  \ V /   | _|    |   /  )_" << endl;
		cout << R"_(  \___|   |_|_| |_|__|_|  |___|            \___/    \_/    |___|   |_|_\  )_" << endl;
		cout << "[BAD ENDING]" << endl << endl;
		system("pause");
		player.end = true;
	}	
}

void resetCards(vector<string> empCards, vector<string> slaveCards)	//hands are reset for both Emp and Slave
{
	//clearing and resetting Emperor hand
	for (int i = 0; i < 5; i++)
	{
		empCards.clear();
		empCards.push_back("Emperor");
		empCards.push_back("Civilian");
		empCards.push_back("Civilian");
		empCards.push_back("Civilian");
		empCards.push_back("Civilian");
	}

	//clearing and resetting Slave hand
	for (int i = 0; i < 5; i++)
	{
		slaveCards.clear();
		slaveCards.push_back("Slave");
		slaveCards.push_back("Civilian");
		slaveCards.push_back("Civilian");
		slaveCards.push_back("Civilian");
		slaveCards.push_back("Civilian");
	}
}

void displayCards(vector<string> empCards, vector<string> slaveCards, Kaiji player)
{
	int counter = 1;
	
	if (player.empTurn == true)
	{
		//displaying Emperor hand
		for (int index = 0; index < 5; index++)
		{
			cout << "[" + empCards[index] + "]	(" << counter << ")" << endl;
			++counter;
		}
		cout << endl;
	}
	else
	{
		//displaying Slave hand
		for (int index = 0; index < 5; index++)
		{
			cout << "[" + slaveCards[index] + "]	(" << counter << ")" << endl;
			++counter;
		}
		cout << endl;
	}
}

void playTurn(vector<string> empCards, vector<string> slaveCards, Kaiji player)
{
	//Cards are shown
	displayCards(empCards, slaveCards, player);

	int choice;
	cout << "Choose a card: ";
	cin >> choice;

	//Will only allow to choose from remaining cards
	if (player.empTurn == true)
	{
		while (choice < 1 || choice > 5)
		{
			cout << "You don't have that card. Choose again: ";
			cin >> choice;
			cout << endl;
		}
	}
	else
	{
		while (choice < 1 || choice > 5)
		{
			cout << "You don't have that card. Choose again: ";
			cin >> choice;
			cout << endl;
		}
	}
	system("pause");
	system("cls");

	player.drawIndex = choice - 1;	//Establishes index number of card for winCheck()

	//Checking if player won
	winCheck(empCards, slaveCards, player);

}

int randomizer(int remainingCards)
{
	srand(time(NULL));
	int randomPull = rand() % remainingCards; //gives index for AI's vector

	return randomPull;		//returns index number for AI's vector
}

void winCheck(vector<string> empCards, vector<string> slaveCards, Kaiji player)	//Includes card deletion
{											//Please don't judge me too harshly for hardcoding this. . .
	int tonegawaIndex;

	if (player.empTurn == true)
	{
		tonegawaIndex = randomizer(5);

		//E > Civ
		if (empCards[player.drawIndex] == "Emperor" && slaveCards[tonegawaIndex] != "Slave")
		{
			cout << R"_(You picked the "Emperor". It is uncontested! You WIN!)_" << endl << endl;
			receiveWinnings(player);
		}
		//Civ = Civ
		else if (empCards[player.drawIndex] == "Civilian" && slaveCards[tonegawaIndex] == "Civilian")
		{
			cout << R"_(You both picked a "Civilian". It is A DRAW. Draw another card.)_" << endl << endl;
			empCards.erase(empCards.begin() + player.drawIndex);
			slaveCards.erase(empCards.begin() + player.drawIndex);
			playTurn(empCards, slaveCards, player);
		}
		//Civ > S
		else if (empCards[player.drawIndex] == "Civilian" && slaveCards[tonegawaIndex] == "Slave")
		{
			cout << R"_(You picked the "Civilian". It beats the "Slave". You WIN!)_" << endl << endl;
			receiveWinnings(player);
		}
		//E < S
		else if (empCards[player.drawIndex] == "Emperor" && slaveCards[tonegawaIndex] == "Slave")
		{
			cout << R"_(Your opponent picked the "Slave". They overthrow your "Emperor". You LOSE!)_" << endl << endl;
			lostMatchup(player);
		}
		else
		{
			winCheck(empCards, slaveCards, player);
		}
	}
	else
	{
		tonegawaIndex = randomizer(5);

		//S > E
		if (slaveCards[player.drawIndex] == "Slave" && empCards[tonegawaIndex] != "Civilian")
		{
			cout << R"_(You picked the "Slave". They overthrow the "Emperor"! You WIN!)_" << endl << endl;
			receiveWinnings(player);
		}
		//Civ = Civ
		else if (slaveCards[player.drawIndex] == "Civilian" && empCards[tonegawaIndex] == "Civilian")
		{
			cout << R"_(You both picked a "Civilian". It is A DRAW.)_" << endl << endl;
			slaveCards.erase(slaveCards.begin() + player.drawIndex);
			empCards.erase(slaveCards.begin() + player.drawIndex);
			playTurn(empCards, slaveCards, player);
		}
		//S < Civ
		else if (slaveCards[player.drawIndex] == "Civilian" && empCards[tonegawaIndex] == "Slave")
		{
			cout << R"_(Your opponent picked the "Civilian". It beats the "Slave". You LOSE!)_" << endl << endl;
			lostMatchup(player);
		}
		//S < Civ
		else if (slaveCards[player.drawIndex] != "Slave" && empCards[tonegawaIndex] == "Emperor")
		{
			cout << R"_(Your opponent picked the "Emperor". It is uncontested. You LOSE!)_" << endl << endl;
			lostMatchup(player);
		}
		else
		{
			winCheck(empCards, slaveCards, player);
		}
	}
}

void noTurns()
{
	cout << "Looks like you've run out of turns. . ." << endl;
	cout << "Your ear survives, but your pride doesn't. You were not able" << endl;
	cout << "to win the 20M yen back, but it's already over for you, Kaiji." << endl << endl;

	cout << R"_(   ___     ___   __  __    ___              ___   __   __   ___     ___   )_" << endl;
	cout << R"_(  / __|   /   \ |  \/  |  | __|            / _ \  \ \ / /  | __|   | _ \  )_" << endl;
	cout << R"_( | (_ |   | - | | |\/| |  | _|            | (_) |  \ V /   | _|    |   /  )_" << endl;
	cout << R"_(  \___|   |_|_| |_|__|_|  |___|            \___/    \_/    |___|   |_|_\  )_" << endl;
	cout << "[MEH ENDING]" << endl << endl;
}

void turnCheck(Kaiji* player)
{
	player->turnCounter++;

	if (player->turnCounter > 3)
	{
		player->empTurn = false;
	}
	else if (player->turnCounter > 6)
	{
		player->turnCounter = 0;
		player->empTurn = true;
	}
}

int main()
{
	Kaiji player;
	Kaiji* players;
	//Cards cards;

	vector<string> empCards;
	vector<string> slaveCards;

	//INTRO
	cout << "Welcome to E Card, Kaiji." << endl << endl;
	cout << "We have attached a machine with a 30mm drill to your ear, just as we agreed." << endl;
	cout << "We will play 12 rounds of E Card. In those 12 rounds, you way bet any length" << endl;
	cout << "of drill. If you win a round, you will receive 100,000 yen. If you lose a round," << endl;
	cout << "the drill moves in deeper by how much you wagered." << endl << endl;
	cout << "As for the cards, Emperor beats the Civilian, a Civilian beats the Slave, and" << endl;
	cout << "the Slave beats the Emperor. Because it is harder to win during the Slave turn," << endl;
	cout << "winning will result in receiving 5 times the amount you bet. Don't worry, though." << endl;
	cout << "Losing will not result in the drill moving 5 times the distance." << endl << endl;
	cout << "Good luck." << endl << endl;
	system("pause");
	Sleep(1000);
	cout << "IT'S TIME TO E-E-E-E-E-E-CAAAAAARD!!";
	Sleep(1500);
	system("cls");

	//Game loop here
	for (int round = 0; round <= 12; round++)	//1 loop = 1 round
	{
		cout << "===== R O U N D   " + round << "  =====" << endl;
		cout << "===== Kaiji =:= " + player.millimeters << " millimeters remaining =:= " + player.money << " yen won =====" << endl << endl;
		resetCards(empCards, slaveCards);
		wager(player);
		playTurn(empCards, slaveCards, player);	//card is drawn, results are shown, end of round

		if (player.end == true)
		{
			return 0;
		}
	}
	
	//No more rounds
	noTurns();
	system("pause");
	return 0;
}