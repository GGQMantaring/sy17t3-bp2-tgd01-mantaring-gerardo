#include <iostream>
#include <string>
#include <time.h>		//This is for randomization
#include <windows.h>	//This is for the timer
#include <limits>		//Next two are for cin.ignore
#include <ios>

using namespace std;

struct Player
{
	int dice[2];
	long long gold = 1000;
	long long bet;
	int score;
};

//Rice God AI
struct AI
{
	int dice[2];
	int score;
};

//Header bar shenanigans
void display(Player &player)
{
	cout << "==================================" << endl;
	cout << "         Gold      " << player.gold << endl;
	cout << "==================================" << endl << endl;
}

//Ex2-1 Betting
void determineBet(Player &player)
{
	cout << "How much are you betting?  ";
	
	//These bets are not allowed
	while (!(cin >> player.bet))
	{
		cin.clear();
		cin.ignore(1000, '\n');
		cout << endl << "Invalid input. Try again. " << endl << endl;
		system("pause");
		system("cls");
		display(player);
		cout << "How much are you betting?  ";
	}

	if (player.bet <= 0 || player.bet > player.gold)
	{
		cout << endl << R"_(The goat next to you frowns.)_" << endl;
		cout << R"_("You can't bet that kind of money. Do it again!")_" << endl << endl;
		system("pause");
		system("cls");
		display(player);
		determineBet(player);
	}
	else
	{
	//The bet is allowed
	player.gold -= player.bet;
	cout << endl;
	system("cls");

	//Printing
	cout << endl << endl << endl << endl;
	cout << "You placed a bet of " << player.bet << " gold." << endl << endl;
	}

}

/* VERSION II FOR EX 2-1
int determineBet(Player &player)
{
	cout << "How much are you betting?  ";

	//These bets are not allowed
	while (!(cin >> player.bet))
	{
		cin.clear();
		cin.ignore(1000, '\n');
		cout << endl << "Invalid input. Try again. " << endl << endl;
		system("pause");
		system("cls");
		display(player);
		cout << "How much are you betting?  ";
	}

	if (player.bet <= 0 || player.bet > player.gold)
	{
		cout << endl << R"_(The goat next to you frowns.)_" << endl;
		cout << R"_("You can't bet that kind of money. Do it again!")_" << endl << endl;
		system("pause");
		system("cls");
		display(player);
		determineBet(player);
	}
	else
	{
		//The bet is allowed
		player.gold -= player.bet;
		cout << endl;
		system("cls");

		//Printing
		cout << endl << endl << endl << endl;
		cout << "You placed a bet of " << player.bet << " gold." << endl << endl;
	}

	return player.bet;
}*/

//Ex2-2 Dice roll randomization
void playerRoll(Player &player)	//Player rolls dice
{
	cout << "You roll the dice. . ." << endl << endl;

	//Rolling...
	for (int i = 0; i < 2; i++)
	{
		Sleep(1000);
		player.dice[i] = rand() % 6 + 1;
		cout << "[ " << player.dice[i] << " ]  ";
	}
	cout << endl << endl;
}

void aiRoll(AI &ai)			//AI rolls dice
{
	int die;

	cout << "The Rice God tosses the dice in the air. . ." << endl << endl;

	//Rolling...
	for (int i = 0; i < 2; i++)
	{
		Sleep(1000);
		ai.dice[i] = rand() % 6 + 1;
		cout << "[ " << ai.dice[i] << " ]  ";
	}
	cout << endl << endl;
}

//Ex2-3 Displaying score and payout
void getResult(Player &player, AI ai)
{
	//Determining score
	player.score = player.dice[0] + player.dice[1];
	ai.score = ai.dice[0] + ai.dice[1];

	//Appropriate consequences
	if (player.score == ai.score)
	{
		cout << "It's a tie!" << endl << "Unfortunately, you don't get anything." << endl << endl;
		player.gold += player.bet;
	}
	else if (player.score == 2)
	{
		cout << "Snake-eyes! You get double your bet back!" << endl << "The Rice God looks thoroughly miffed." << endl << endl;
		player.gold += (player.bet * 3);
		player.bet *= 2;
		cout << "You collect " << player.bet << " gold, which you slide into your pouch." << endl << endl;
	}
	else if (ai.score == 2)
	{
		cout << "Oh, that's not too good. Snake-eyes beats everything, including you." << endl << "The Rice God laughs at your face." << endl << endl;
		cout << "You lose " << player.bet << " gold." << endl << endl;
	}
	else if (player.score > ai.score)
	{
		cout << "Hey... you actually won that round! The goat is not displeased." << endl << endl;
		player.gold += (player.bet * 2);
		cout << "You win " << player.bet << " gold." << endl << endl;
	}
	else
	{
		cout << "I know that dice are random, so I can't even tell you to do better" << endl;
		Sleep(3000);
		cout << "because you literally can't.";
		Sleep(1500);
		cout << " Better luck, I guess." << endl << endl;
		cout << "You lose " << player.bet << " gold." << endl << endl;
	}
}


//Ex2-4
void playRound(Player &player, AI &ai)
{
	display(player);
	determineBet(player);
	playerRoll(player);
	system("pause");
	cout << endl;
	aiRoll(ai);
	system("pause");
	system("cls");	
	cout << endl << endl << endl << endl;
	getResult(player, ai);
	system("pause");
	system("cls");
}

int main()
{
	Player player;
	AI ai;
	int bet;
	
	//I like flavour text
	cout << "========= W E L C O M E ==========" << endl;
	cout << R"_( ____  ___    __ __ __  __  _____  )_" << endl;
	cout << R"_(((    ||=||  ((  || ||\\|| ((   )) )_" << endl;
	cout << R"_( \\__ || || \_)) || || \||  \\_//  )_" << endl << endl;
	cout << "You are gambling against a Rice God." << endl;
	cout << "He takes a look at your pouch of gold and smiles." << endl;
	cout << R"_("You're now trapped in this room forever)_" << endl;
	cout << R"_(until you run out of money." I guess that's it then.)_" << endl;
	cout << "Play on." << endl << endl;
	system("pause");
	system("cls");

	srand(time(NULL));

	//Actual bit of code that loops
	while (player.gold != 0)
	{
		playRound(player, ai);
	}

	display(player);
	cout << "The goat by your side looks at you with some disdain." << endl;
	cout << R"_("That's it," he says. "You're all done, for you haven't go any money.")_" << endl;
	cout << "You woefully stand and leave the pantheon. The Rice God laughs his bigas off." << endl << endl;

	system("pause");
	return 0;
}