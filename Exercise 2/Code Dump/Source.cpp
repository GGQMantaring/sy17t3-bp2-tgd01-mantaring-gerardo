
//FUNCTION TO DETERMINE BET
int bet(int gold)
{
int bet;

cout << "How much are you betting?   ";
cin >> bet;

return gold;
}


//FUNCTION TO TAKE, DEDUCT, AND RETURN BET VALUE
int bet(int& gold)
{
int bet;

cout << "How much are you betting?    ";
cin >> bet;
gold -= bet;
cout << endl << endl;
system("pause");
system("cls");

//Printing
cout << "You placed a bet of " << bet << " gold." << endl;

return gold;
}

//Ex2-1 Betting
void determineBet(Player &player)
{
	//Numbers only, please
	while (!(cin >> player.bet))
	{
		cin.clear();
		cin.ignore(numeric_limits<streamsize>::max()); // ignore bad input
		determineBet(player);
	}
	
	//These bets are not allowed
	if (player.bet <= 0 || player.bet > player.gold)
	{
		cout << endl << R"_(The goat next to you frowns. "You can't bet that kind of money. Do it again!")_" << endl << endl;
		system("cls");
		display(player);
		determineBet(player);
	}
	
	//The bet is allowed
	player.gold -= player.bet;
	cout << endl;
	system("cls");

	//Printing
	cout << endl << endl << endl << endl;
	cout << "You placed a bet of " << player.bet << " gold." << endl << endl;
}


void bet(int& gold, int& bet)
{
cout << "How much are you betting?    ";
cin >> bet;
gold -= bet;
cout << endl << endl;
system("pause");
system("cls");

//Printing
cout << "You placed a bet of " << bet << " gold." << endl;
}
