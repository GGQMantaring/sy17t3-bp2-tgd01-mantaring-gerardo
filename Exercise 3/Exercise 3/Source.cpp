#include <iostream>
#include <string>
#include <time.h>		//These three are for randomization
#include <stdio.h>
#include <stdlib.h>
#include <windows.h>	//This is for the timer

using namespace std;

//Note: I REALLY like flavor text

struct Player
{
	long long gold = 50;
};

//Ex 3-1
struct Item //Struct specified from instructions
{
	string name;
	int gold;
};

void GenerateRandItem(Item &item)
{
	string obj[5]{ "Mithril Ore", "Sharp Talon", "Thick Leather", "Jellopy", "Cursed Stone" };
	int value[5]{ 100,50,25,5,0 };

	srand(time(NULL));	//rand seed initialization
	int randID = rand() % 5;

	item.name = obj[randID];	//assigning name and value to 'item'
	item.gold = value[randID];
}

//Ex 3-2
void EnterDungeon(Item item, Player &player)
{
	Item *loot;
	Player *lootie;
	long long total = 0;
	int	mult = 1;
	char ans;
	bool check = false;	//check if user will leave dungeon; assumed false
	
	loot = &item;
	lootie = &player;

	lootie->gold -= 25;
	cout << "============================" << endl;
	cout << "  Lootie's Gold  " << lootie->gold << endl;
	cout << "============================" << endl << endl;
	cout << "You toss 25 gold coins into the slot next to the stone slab blocking" << endl;
	cout << "the entrance. With a deep rumbling, the door sinks into darkness and" << endl;
	cout << "a line of torches along the walls light up a path." << endl << endl;
	cout << "You take a torch from the wall, and head into the depths." << endl << endl;
	system("pause");
	system("cls");

	//Dungeon crawl loop
	do
	{		
		GenerateRandItem(item);			//generate item
		
		cout << R"_(~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~)_" << endl;
		cout << "     Total gold looted:  " << total << endl;
		cout << R"_(~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~)_" << endl << endl;
		cout << "You start looting. . ." << endl << endl;
		Sleep(1500);
		system("cls");
		
		//check if Cursed Stone was acquired

		if (loot->gold == 0)
		{
			//You got cursed!
			system("cls");
			cout << R"_(~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~)_" << endl;
			cout << "     Total gold looted:  0" << endl;
			cout << R"_(~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~)_" << endl << endl;
			cout << "Oof! You picked up a Cursed Stone. You faint and fall to the ground." << endl << endl;
			cout << "The dungeon kobolds ironically loot you for all items and toss your body" << endl;
			cout << "just outside of the dungeon. You earned nothing from that run." << endl;
			cout << "When you wake up, the door to the dungeon is closed again." << endl << endl;
			Sleep(3000);
			system("pause");
			system("cls");
			check = true;	//You are out of the dungeon; pay again
		}
		else
		{
			total += ((loot->gold)*mult);	//calculate current total looted
			cout << R"_(~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~)_" << endl;
			cout << "     Total gold looted:  " << total << endl;
			cout << R"_(~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~)_" << endl << endl;
			cout << "You found a " << loot->name << "!" << endl;
			cout << "It's worth " << loot->gold << " (x " << mult << ") gold." << endl << endl;
			cout << "Do you want to loot again? (y/n) ";
			
			cin >> ans;

			switch (ans)
			{
			case 'y':
			case'Y':
				mult++;							//increase multiplier
				system("cls");
				break;
			case'n':	//Player chooses to exit dungeon
			case'N':
				system("cls");
				cout << "It's okay to chicken out sometimes." << endl;
				cout << "It doesn't mean you're any less of a looter." << endl;
				cout << "Maybe next time. . ." << endl << endl;
				cout << R"_(~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~)_" << endl;
				cout << "You collect " << total << " gold from this run." << endl;
				cout << R"_(~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~)_" << endl << endl;
				lootie->gold += total;
				Sleep(2000);
				cout << "As you leave the dungeon, the dungeon locks you out." << endl;
				cout << "Looks like if you want to get in, you're going to have to pay again." << endl << endl;
				system("pause");
				system("cls");
				check = true;
				break;
			default:
				system("cls");
				cin.clear();
				cin.ignore(1000, '\n');
				cout << endl << "Sorry, I didn't understand what you meant by that." << endl << endl;
				cout << R"_(I'm going to assume you meant "yes", so I'm making you loot again.)_";
				Sleep(3500);
				system("cls");
			}			
		}
		
	} while (!check);

}

	

//Ex 3-3
int main()
{
	Player *lootie, player;
	Item item;
	bool conditions = false;	//used to check if game conditions are met; assumed false

	lootie = &player;

	cout << R"_(          ================================================)_" << endl;
	cout << R"_(           ~|~|_  _    /\  _|   _  _ _|_   _ _  _   _  |` )_" << endl;
	cout << R"_(            | | |(/_  /~~\(_|\/(/_| | ||_|| (/__\  (_)~|~ )_" << endl;
	cout << R"_(                        _,   _,  _, ___ _ __,             )_" << endl;
	cout << R"_(             = =        |   / \ / \  |  | |_        = =   )_" << endl;
	cout << R"_(             = =        | , \ / \ /  |  | |         = =   )_" << endl;
	cout << R"_(                        ~~~  ~   ~   ~  ~ ~~~             )_" << endl;
	cout << R"_(          ================================================)_" << endl << endl;
	cout << R"_(You are Lootie, the "World's Best Dungeon Looter". Today, you've returned to)_" << endl;
	cout << "Dungeon Ek'sersaistri to fill your coffers with more loot. You've brought 50 GOLD" << endl;
	cout << "with you, which should be more than enough to cover the 25 GOLD entrance fee of " << endl;
	cout << "the dungeon. That is. . . if you're willing to risk all your loot with a CURSED STONE." << endl;
	cout << "Picking one of them up will cause you to horribly faint, making you lose all the gold" << endl;
	cout << "during that run. If you do stay and loot though, the cost of the items will continue" << endl;
	cout << "to grow larger and larger." << endl << endl;
	cout << "Will you loot your way to riches, or will Greed claim you this time?" << endl << endl;
	system("pause");
	system("cls");
		
	do
	{
		EnterDungeon(item, player);
		
		//Check if end-game conditions are met
		if (lootie->gold < 25)			//Lose Condition
		{
			cout << "             ============================" << endl;
			cout << "                   Lootie's Gold  " << lootie->gold << endl;
			cout << R"_(      ==========================================)_" << endl;
			cout << R"_(                          __                  _ )_" << endl;
			cout << R"_(      /\_/\___  _   _    / /  ___  ___  ___  / \)_" << endl;
			cout << R"_(      \_ _/ _ \| | | |  / /  / _ \/ __|/ _ \/  /)_" << endl;
			cout << R"_(       / \ (_) | |_| | / /__| (_) \__ \  __/\_/ )_" << endl;
			cout << R"_(       \_/\___/ \__,_| \____/\___/|___/\___\/   )_" << endl;
			cout << R"_(      ==========================================)_" << endl << endl;
			cout << "Well, that's no good. It looks like you don't have enough" << endl;
			cout << "gold with you. This means you can't pay the fee." << endl;
			cout << "Looks like you're done looting here." << endl << endl;
			conditions = true;
		}
		else if (lootie->gold >=500)	//Win Condition
		{
			cout << "             ============================" << endl;
			cout << "                   Lootie's Gold  " << lootie->gold << endl;
			cout << R"_(      ========================================)_" << endl;
			cout << R"_(                         __    __ _         _ )_" << endl;
			cout << R"_(       /\_/\___  _   _  / / /\ \ (_)_ __   / \)_" << endl;
			cout << R"_(       \_ _/ _ \| | | | \ \/  \/ / | '_ \ /  /)_" << endl;
			cout << R"_(        / \ (_) | |_| |  \  /\  /| | | | /\_/ )_" << endl;
			cout << R"_(        \_/\___/ \__,_|   \/  \/ |_|_| |_\/   )_" << endl;
			cout << R"_(      ========================================)_" << endl << endl;
			cout << "Great! You've earned just enough gold for another night of" << endl;
			cout << "mindless debauchery. You decide you're done looting for today," << endl;
			cout << "and that it's instead time to get pissing drunk." << endl << endl;
			conditions = true;
		}
	} while (conditions == false);		//checks if you still have gold to pay
	
	
	system("pause");
	return 0;
}