#pragma once
#include <string>

using namespace std;

class Character
{
private:
	string name;	
	int HP, POW, VIT, AGI, DEX;
	
public:
	Character();
	~Character();
		
	string jobName;
	int jobID = 0;
	bool canGoOn = true;

	//Creating a character
	void createPlayer();
	void assignEnemyValues(int floorNum);
	void assignValues();

	//Display stats
	void displayStats(Character* enemy);
	
	//Battle functions
	//	Damage Calculations
	int damage(Character* enemy);
	bool checkBonus(Character* enemy);
	bool confirmHit(Character* enemy);

	void attack(Character* enemy, bool& playerTurn); //*

	//	Levelling up
	void endOfFight(Character* enemy);




};
