#include <iostream>
#include <ctime>
#include <string>
#include <vector>
#include "Character.h"
#include <cstdlib>

using namespace std;

int main()
{
	srand(time(NULL));
	/*
	- create character
	- function to create random enemy
	- adventure loop
	- lose condition break
	*/
	int floorNum = 1;
	bool playerTurn = true;
	Character* hero = new Character();
		
	hero->createPlayer();
	
	do
	{
		//Creating a new enemy
		Character* villain = new Character();
		villain->assignEnemyValues(floorNum);
		cout << "A rogue " << villain->jobName << " enemy appears!" << endl << endl;

		//Battle loop
		do
		{
			hero->displayStats(villain);
			hero->attack(villain, playerTurn);
			villain->attack(hero, playerTurn);
		} while (hero->canGoOn == true && villain->canGoOn == true);

		if (villain->canGoOn == false)
		{
			hero->endOfFight(villain);
			delete villain;
			floorNum++;
			system("pause");
		}
		else if (hero->canGoOn == false)
		{
			cout << "You died!" << endl;
		}

	} while (hero->canGoOn == true);	

	system("pause");
	return 0;
}