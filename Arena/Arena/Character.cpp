#include <iostream>
#include <ctime>
#include "Character.h"

using namespace std;

Character::Character()
{
	//Assigning base values
	HP = 10;
	POW = 10;
	VIT = 10;
	AGI = 10;
	DEX = 10;
}


Character::~Character()
{
}

int Character::damage(Character * enemy)
{
	//Calculates damage
	int dmg;
	
	dmg = (this->POW - enemy->VIT);	//formula provided
	
	//Applies bonus damage if applicable
	if (this->checkBonus(enemy))
	{
		dmg *= 15;
		dmg /= 10;
	}

	//Clamping minimum like a person who doesn't
	//know how to use std::clamp
	if (dmg < 1)
	{
		dmg = 1;
	}

	return dmg;
}

bool Character::checkBonus(Character * enemy)
{
	if (this->jobID == 1 && enemy->jobID == 2)
	{
		return true;
	}
	else if (this->jobID == 2 && enemy->jobID == 3)
	{
		return true;
	}
	else if (this->jobID == 3 && enemy->jobID == 1)
	{
		return true;
	}
	else
	{
		return false;
	}
}

bool Character::confirmHit(Character * enemy)
{
	int accuracy, chance;

	accuracy = (this->DEX / enemy->AGI) * 100;	//formula provided
	//Clamping hardcoded because clamp isn't working for some reason
	if (accuracy < 20)
	{
		accuracy = 20;
	}
	else if (accuracy > 80)
	{
		accuracy = 80;
	}

	//Chance roll
	chance = rand() % 101;

	//Compare chance and accuracy
	if (chance >= accuracy)
	{
		return true;
	}
	else
	{
		return false;
	}
}

void Character::attack(Character * enemy, bool& playerTurn)
{
	if (this->HP > 0)
	{
		
		int damage;

		//Attack flow
		if (playerTurn == true)
		{
			int index = rand() % 5;	//	Rolling for random verb
			cout << this->name << " bamboozles with their weapon and some rude gestures!" << endl << endl;
			this->confirmHit(enemy);

			if (this->confirmHit(enemy) == true)
			{
				damage = this->damage(enemy);
				cout << "It's a hit! " << this->name << " dealt " << damage << " damage." << endl << endl;
				enemy->HP -= damage;
				cout << enemy->jobName << " remaining HP: " << enemy->HP << "." << endl << endl;
				system("pause");
				cout << endl << endl;
			}
			else
			{
				cout << "O O F , it's a miss!" << endl << endl;
				system("pause");
				cout << endl << endl;
			}

			playerTurn = false;
		}
		else
		{
			int index = rand() % 5;	//	Rolling for random verb
			cout << this->jobName << " shimmyshoos with their weapon and some rude gestures!" << endl << endl;
			this->confirmHit(enemy);

			if (this->confirmHit(enemy) == true)
			{
				damage = this->damage(enemy);
				cout << "The opponent strikes! " << this->jobName << " dealt " << damage << " damage." << endl << endl;
				enemy->HP -= damage;
				cout << enemy->name << " remaining HP: " << enemy->HP << "." << endl << endl;
				system("pause");
				cout << endl << endl;
			}
			else
			{
				cout << "W H I F F !!" << endl << endl;
				system("pause");
				cout << endl << endl;
			}

			playerTurn = true;
		}		
	}
	else
	{
		canGoOn = false;
	}
}

void Character::endOfFight(Character* enemy)
{
	cout << "You leveled up!" << endl << endl;	
	
	//Add values
	
	if (enemy->jobID == 1)
	{
		this->HP += 3;
		this->VIT += 3;

		//Display values
		cout << "HP:  " << this->HP << "     ( +3 )" << endl;
		cout << "VIT:  " << this->VIT << "     ( +3 )" << endl;
		cout << "AGI:  " << this->AGI << "     ( +0 )" << endl;
		cout << "DEX:  " << this->DEX << "     ( +0 )" << endl;
		cout << "POW:  " << this->POW << "     ( +0 )" << endl << endl;
	}
	else if (enemy->jobID == 2)
	{
		this->AGI += 3;
		this->DEX += 3;

		//Display values
		cout << "HP:  " << this->HP << "     ( +0 )" << endl;
		cout << "VIT:  " << this->VIT << "     ( +0 )" << endl;
		cout << "AGI:  " << this->AGI << "     ( +3 )" << endl;
		cout << "DEX:  " << this->DEX << "     ( +3 )" << endl;
		cout << "POW:  " << this->POW << "     ( +0 )" << endl << endl;
	}
	else if (enemy->jobID = 3)
	{
		this->POW += 5;

		//Display values
		cout << "HP:  " << this->HP << "     ( +0 )" << endl;
		cout << "VIT:  " << this->VIT << "     ( +0 )" << endl;
		cout << "AGI:  " << this->AGI << "     ( +0 )" << endl;
		cout << "DEX:  " << this->DEX << "     ( +0 )" << endl;
		cout << "POW:  " << this->POW << "     ( +5 )" << endl << endl;
	}

	cout << endl << endl;
	
}

void Character::createPlayer()
{
	cout << "Hail, friend! What is your name? ";
	cin >> this->name;
	cout << endl << "You are " << this->name << ", about to embark upon an endless quest." << endl;
	cout << "In fact, the dungeon won't be kind at all, and it will pretty much just keep going" << endl;
	cout << "until you die. But that is the life of an adventurer like you, is it not?" << endl << endl;
	system("pause");
	system("cls");

	cout << "Warrior    (1)" << endl;
	cout << "Assassin   (2)" << endl;
	cout << "Mage       (3)" << endl << endl;
		
	while (this->jobID < 1 || this->jobID > 3)
	{
		cout << "Now then, what is your profession? (ENTER A NUMBER)  ";
		cin >> this->jobID;

		if (this->jobID < 1 || this->jobID > 3)
		{
			cout << "That is not a valid option. Please try again." << endl << endl;
		}
		else
		{
			this->assignValues();
			cout << endl << "Splendid! You are a " << this->jobName << ". Good luck with your quest." << endl;
			cout << "May Death come quick to whom it chooses. Do not forget to offer each corpse to The RNG." << endl << endl;
			system("pause");
			system("cls");
		}
	}
}

void Character::assignEnemyValues(int floorNum)
{
	//Determining random this job class
	this->jobID = rand() % 3 + 1;
	
	//Assigning this's job-specific values
	this->assignValues();

	//Semi-Linearly scaling this to dungeon level
	if (this->jobID == 1)
	{
		this->jobName = "Warrior";
		this->HP += (3 * floorNum);
		this->VIT += (3 * floorNum);
		this->AGI += rand() % 2 + floorNum;
		this->DEX += rand() % 2 + floorNum;
		this->POW += rand() % 2 + floorNum;
	}
	else if (this->jobID == 2)
	{
		this->jobName = "Assassin";
		this->AGI += (3 * floorNum);
		this->DEX += (3 * floorNum);
		this->HP += rand() % 2 + floorNum;
		this->VIT += rand() % 2 + floorNum;
		this->POW += rand() % 2 + floorNum;
	}
	else if (this->jobID = 3)
	{
		this->jobName = "Mage";
		this->POW += (5 * floorNum);
		this->HP += rand() % 2 + floorNum;
		this->VIT += rand() % 2 + floorNum;
		this->AGI += rand() % 2 + floorNum;
		this->DEX += rand() % 2 + floorNum;
	}
}


void Character::assignValues()
{
	//Assigning job-specific values
	if (this->jobID == 1)
	{
		this->jobName = "Warrior";
		this->HP += 3;
		this->VIT += 3;
	}
	else if (this->jobID == 2)
	{
		this->jobName = "Assassin";
		this->AGI += 3;
		this->DEX += 3;
	}
	else if (this->jobID = 3)
	{
		this->jobName = "Mage";
		this->POW += 5;
	}
}

void Character::displayStats(Character* enemy)
{
	cout << this->name << ":            " << enemy->jobName << ":" << endl;
	cout << "HP:   " << this->HP << "         " << "HP:   " << enemy->HP << endl;
	cout << "VIT:   " << this->VIT << "         " << "VIT:   " << enemy->VIT << endl;
	cout << "AGI:   " << this->AGI << "         " << "AGI:   " << enemy->AGI << endl;
	cout << "DEX:   " << this->DEX << "         " << "DEX:   " << enemy->DEX << endl;
	cout << "POW:   " << this->POW << "         " << "POW:   " << enemy->POW << endl;
	cout << endl << endl;
}
