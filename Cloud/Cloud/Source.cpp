#include <iostream>
#include <string>
#include <vector>

using namespace std;

class Character
{
public:
	string name;
	unsigned long long maxEXP;
	unsigned int level;
	unsigned int maxHP;
	unsigned int maxMP;
	unsigned int LimLevel;
	unsigned int maxLimitBar;

	string statusEffect;

	//Assigns stats for charater
	void statValues(statusValues* stat);
	void combatValues(combatValues* stat);

	//Assigns equipment on character
	void equippedWeapon(Weapon* name);
	void equippedArmor(Armor* name);
	void equippedAccessory(Accessory* name);

	//Actions of character
	void battleCommands(BattleCommands& action);

	//Affliction, if any
	void displayStatusEffect(StatusEffect& name);

	//Character's display picture
	void characterImage(Character* name);	//I can't tell if this is proper


} player;

struct statusValues
{
	int str;
	int dex;
	int vit;
	int mag;
	int spi;
	int luk;
};

struct combatValues
{
	int attack;
	int atkPercent;
	int defense;
	int defPercent;
	int magAtk;
	int magDef;
	int magDefPercent;
};

class BattleCommands
{
public:
	string attackName;
	vector<string> magic;
	vector<string> items;

	void materiaMagic(Weapon& matWpn, Armor& matArm);
	void wait();
	void defend();
	void change();
};

class Weapon
{
public:
	string name;
	string materia[8];

	void intrinsicEffects();
	void equippedMateria(Materia& name);
};

class Armor
{
public:
	string name;
	string materia[8];
	
	void intrinsicEffects();
	void equippedMateria(Materia& name);
};

class Accessory
{
public:
	string name;

	void intrinsicEffects();
};

class Materia
{
public:
	string name;

	void effect();
};

class StatusEffect
{
public:
	string name;

	void effect();
};